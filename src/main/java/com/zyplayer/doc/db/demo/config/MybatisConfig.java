package com.zyplayer.doc.db.demo.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jta.atomikos.AtomikosDataSourceBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * mybatis数据库配置，配置有mysql和sqlsever两种数据库的例子
 * 
 * @author 暮光：城中城
 * @since 2018年8月8日
 */
@Configuration
public class MybatisConfig {

	/**
	 * mysql数据库配置
	 */
	@Configuration
	@EnableTransactionManagement
	static class UserInfoMybatisDbConfig {

		@Value("${mysql.datasource.driverClassName}")
		private String driverClassName;
		@Value("${mysql.datasource.url}")
		private String datasourceUrl;
		@Value("${mysql.datasource.username}")
		private String datasourceUsername;
		@Value("${mysql.datasource.password}")
		private String datasourcePassword;

		@Primary
		@Bean(name = "userInfoDatasource")
		public DataSource userInfoDatasource() {
			Properties xaProperties = new Properties();
			xaProperties.setProperty("driverClassName", driverClassName);
			xaProperties.setProperty("url", datasourceUrl);
			xaProperties.setProperty("username", datasourceUsername);
			xaProperties.setProperty("password", datasourcePassword);
			xaProperties.setProperty("maxActive", "500");
			xaProperties.setProperty("testOnBorrow", "true");
			xaProperties.setProperty("testWhileIdle", "true");
			xaProperties.setProperty("validationQuery", "select 'x'");

			AtomikosDataSourceBean xaDataSource = new AtomikosDataSourceBean();
			xaDataSource.setXaProperties(xaProperties);
			xaDataSource.setXaDataSourceClassName("com.alibaba.druid.pool.xa.DruidXADataSource");
			xaDataSource.setUniqueResourceName("userInfoDatasource");
			xaDataSource.setMaxPoolSize(500);
			xaDataSource.setMinPoolSize(1);
			xaDataSource.setMaxLifetime(60);
			return xaDataSource;
		}

		@Primary
		@Bean(name = "userInfoSqlSessionFactory")
		public SqlSessionFactoryBean userInfoSqlSessionFactory() throws Exception {
			SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
			sqlSessionFactoryBean.setDataSource(userInfoDatasource());
			return sqlSessionFactoryBean;
		}
	}

	/**
	 * sqlsever数据库配置
	 */
	@Configuration
	@EnableTransactionManagement
	static class OrderInfoMybatisDbConfig {

		@Value("${sqlsever.datasource.driverClassName}")
		private String driverClassName;
		@Value("${sqlsever.datasource.url}")
		private String datasourceUrl;
		@Value("${sqlsever.datasource.username}")
		private String datasourceUsername;
		@Value("${sqlsever.datasource.password}")
		private String datasourcePassword;

		@Bean(name = "orderInfoDatasource")
		public DataSource orderInfoDatasource() {
			Properties xaProperties = new Properties();
			xaProperties.setProperty("driverClassName", driverClassName);
			xaProperties.setProperty("url", datasourceUrl);
			xaProperties.setProperty("username", datasourceUsername);
			xaProperties.setProperty("password", datasourcePassword);
			xaProperties.setProperty("maxActive", "500");
			xaProperties.setProperty("testOnBorrow", "true");
			xaProperties.setProperty("testWhileIdle", "true");
			xaProperties.setProperty("validationQuery", "select 'x'");

			AtomikosDataSourceBean xaDataSource = new AtomikosDataSourceBean();
			xaDataSource.setXaProperties(xaProperties);
			xaDataSource.setXaDataSourceClassName("com.alibaba.druid.pool.xa.DruidXADataSource");
			xaDataSource.setUniqueResourceName("orderInfoDatasource");
			xaDataSource.setMaxPoolSize(500);
			xaDataSource.setMinPoolSize(1);
			xaDataSource.setMaxLifetime(60);
			return xaDataSource;
		}

		@Bean(name = "orderInfoSqlSessionFactory")
		public SqlSessionFactoryBean orderInfoSqlSessionFactory() throws Exception {
			SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
			sqlSessionFactoryBean.setDataSource(orderInfoDatasource());
			return sqlSessionFactoryBean;
		}
	}

}
