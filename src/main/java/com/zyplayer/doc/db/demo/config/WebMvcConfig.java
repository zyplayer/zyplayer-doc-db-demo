package com.zyplayer.doc.db.demo.config;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.zyplayer.doc.db.framework.configuration.EnableDocDb;
import com.zyplayer.doc.db.framework.db.bean.DatabaseRegistrationBean;

/**
 * bean声明注入和开启注解
 * 
 * @author 暮光：城中城
 * @since 2018年8月8日
 */
@Component
@EnableDocDb// 开启文档注解
@Configuration
public class WebMvcConfig {
	
	// mysql数据源例子
	@Resource(name = "userInfoDatasource")
	private DataSource userInfoDatasource;
	// sqlsever数据源例子
	@Resource(name = "orderInfoDatasource")
	private DataSource orderInfoDatasource;

	/**
	 * 声明文档所需的bean
	 * 
	 * @author 暮光：城中城
	 * @since 2018年8月8日
	 * @return
	 */
	@Bean
	public DatabaseRegistrationBean databaseRegistrationBean() {
		DatabaseRegistrationBean bean = new DatabaseRegistrationBean();
		List<DataSource> dataSourceList = new LinkedList<>();
		dataSourceList.add(userInfoDatasource);
		dataSourceList.add(orderInfoDatasource);
		bean.setDataSourceList(dataSourceList);
		return bean;
	}
}
